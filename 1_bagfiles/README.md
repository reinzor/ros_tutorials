# Manipulating bag files 

1. Matlab API (https://nl.mathworks.com/help/robotics/examples/work-with-rosbag-logfiles.html) - See `plot_bag.m`
2. Python API (http://wiki.ros.org/rosbag/Code%20API) - See `plot_bag.py`
3. C++ API (http://wiki.ros.org/rosbag/Code%20API) - See examples in url
4. Java API (https://github.com/swri-robotics/bag-reader-java) - See examples in url

