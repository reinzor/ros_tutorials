#!/usr/bin/env python

# Import dependencies
import rosbag
import matplotlib.pyplot as plt

# Retrieve information from the rosbag
bag = rosbag.Bag('example.bag')

# Select a subset of the messages, filtered by topic.
msgs = [msg for topic, msg, t in bag.read_messages(topics=['/et1/vehicle/odom'])]

# Get the required arrays for the plot
x = [msg.pose.pose.position.x for msg in msgs]
y = [msg.pose.pose.position.y for msg in msgs]

# Plot
plt.plot(x, y, linewidth=2.0)
plt.xlabel('x')
plt.ylabel('y')
plt.title('Odometry position')
plt.show()
