% Retrieve information from the rosbag
bag = rosbag('./example.bag');

% Select a subset of the messages, filtered by time and topic.
odom_selection = select(bag, 'Time', [bag.StartTime bag.EndTime], 'Topic', '/et1/vehicle/odom');
odom_msgs = odom_selection.readMessages();

% Get the required arrays for the plot
x = cellfun(@(msg) msg.Pose.Pose.Position.X, odom_msgs)
y = cellfun(@(msg) msg.Pose.Pose.Position.Y, odom_msgs)

% Plot
plot(x,y)
xlabel('x')
ylabel('y')
title('Odometry position')