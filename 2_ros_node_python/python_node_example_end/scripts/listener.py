#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32, Bool

def callback(data):
    even = data.data % 2
    print "%f is %s" % (data.data, "even" if even else "odd")
    pub.publish(Bool(data=even))

if __name__ == '__main__':
    rospy.init_node('listener')
    rospy.Subscriber('counter', Float32, callback)
    pub = rospy.Publisher('result', Bool, queue_size=0)
    rospy.spin()
