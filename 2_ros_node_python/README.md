# Python node

The tutorial result can be found here: [python_node_example_end](python_node_example_end)

## Tutorial

### Part 1

Create catkin package, for details: http://wiki.ros.org/rospy_tutorials/Tutorials/Makefile

    catkin_create_pkg python_node_example_start rospy std_msgs

Go to your newly created package:

    cd python_node_example_start

Look at the package directory, it does contain a `CMakeLists.txt` and a `package.xml` and a `src` folder, delete that one since we are going to make a script

    rm -rf src

Make a scripts directory, and enter this directory

    mkdir scripts
    cd scripts

Now we are going to make a simple Publisher/Subcriber as described here: http://wiki.ros.org/rospy_tutorials/Tutorials/WritingPublisherSubscriber

    wget https://raw.github.com/ros/ros_tutorials/kinetic-devel/rospy_tutorials/001_talker_listener/talker.py # This will download the talker example

Make the talker.py executable:

    chmod +x talker.py # Where +x says, add the executable flag
    
First start a roscore

    roscore

Now we can run the executable

    rosrun python_node_example_start talker.py

Inspect the ROS network using the `rosnode` and `rostopic` commands:

    rosnode list
    rosnode info [node_name]
    rostopic list
    rostopic echo

You will see that the hello world message is being published

### Part 2

Now modify the script and make it publish a std_msgs/Float32 with an increasing counter @ 1hz on the topic `counter`

    # Do some work yourself :)

If you are done with this, run your node again:

    rosrun python_node_example_start talker.py

Now inspect the ROS network and you will see the `counter` topic, you can inspect this topic:

    rostopic echo /counter

### Part 3

Now we would like to create another node that subscribes to this `/counter` topic, and print whether the counter is even or odd.

You can start with this template:

    wget https://raw.github.com/ros/ros_tutorials/kinetic-devel/rospy_tutorials/001_talker_listener/listener.py

Als make this listener.py executable:

    chmod +x listener.py 

Now change the listener.py code to realize the behavior described above

    # Do some work yourself

Now run the node you just created in another terminal:

    rosrun python_node_example_start listener.py

Now update the code and make the node publish a `std_msgs/Bool` over the `/result` topic; `True' if the value is `even` and a `False` if the message is `odd`:

    # Do some work yourself

And now rerun the node and display the message:

    rosrun python_node_example_start listener.py
    rostopic echo /result
    
Good job, you finished this tutorial! Now go connect the newly created ROS node with some other ROS node that you have created. Without connecting things to each other, there is no need using ROS!




