# C++ node

The tutorial result can be found here: [cpp_node_example_end](cpp_node_example_end)

## Tutorial

### Part 1

Create catkin package:

    catkin_create_pkg cpp_node_example_start roscpp std_msgs

Go to your newly created package:

    cd cpp_node_example_end

Look at the package directory, it does contain a `CMakeLists.txt`, a `package.xml`, a `src` directory and an `include` folder, delete that one since we are going to make any cpp headers

    rm -rf include

Now go to the `src` directory:

    cd src

Now we are going to make a simple Publisher/Subcriber as described here: http://wiki.ros.org/roscpp_tutorials/Tutorials/WritingPublisherSubscriber

    wget https://raw.github.com/ros/ros_tutorials/kinetic-devel/roscpp_tutorials/talker/talker.cpp # This will download the talker example

Make the talker executable does not hold here since we have to compile it (it's c++). We have to add the compilation target to the CMakeLists.txt of this package:

    gedit ../CMakeLists.txt

Now go to the the line that says: `Declare a C++ executable` and uncomment the lines below (the `add_executable` line) and change the reference to the cpp file and the executable name:

    add_executable(${PROJECT_NAME}_talker_node src/talker.cpp)

Next, make sure we link against the catkin_libraries with this executable so that we can find our includes (`std_msgs` and `roscpp`). 

Go to `Specify libraries to link a library or executable target against` and set-up the linking for the executable:

    target_link_libraries(${PROJECT_NAME}_talker_node ${catkin_LIBRARIES})

Now save and close `gedit` and compile the catkin workspace:

    catkin build

Now we can run the executable

    rosrun cpp_node_example_start cpp_node_example_start_talker_node

Inspect the ROS network using the `rosnode` and `rostopic` commands:

    rosnode list
    rosnode info [node_name]
    rostopic list
    rostopic echo

You will see that the hello world message is being published

### Part 2

Now modify the `talker.cpp` and make it publish a std_msgs/Float32 with an increasing counter @ 1hz on the topic `counter`

    # Do some work yourself :)

If you are done with this, run your node again:

    rosrun cpp_node_example_start cpp_node_example_start_talker_node

Now inspect the ROS network and you will see the `counter` topic, you can inspect this topic:

    rostopic echo /counter

### Part 3

Now we would like to create another node that subscribes to this `/counter` topic, and print whether the counter is even or odd.

You can start with this template:

    wget https://raw.githubusercontent.com/ros/ros_tutorials/kinetic-devel/roscpp_tutorials/listener/listener.cpp

Now again add this to the CMakelists.txt as a compile target and make sure it links against the catkin_libraries (see part 1).

Now change the listener.cpp code to realize the behavior described above

    # Do some work yourself

Now run the node you just created in another terminal:

    rosrun cpp_node_example_start cpp_node_example_start_listener_node

Now update the code and make the node publish a `std_msgs/Bool` over the `/result` topic; `True' if the value is `even` and a `False` if the message is `odd`:

    # Do some work yourself

And now rerun the node and display the message:

    rosrun cpp_node_example_start cpp_node_example_start_listener_node
    rostopic echo /result
    
Good job, you finished this tutorial! Now go connect the newly created ROS node with some other ROS node that you have created. Without connecting things to each other, there is no need using ROS!
