# Simulink node

The tutorial result can be found here: [simulink_node_example_end](simulink_node_example_end)

## Prerequisites 

Matlab Simulink 2016a

## Tutorial

### Part 1

Open matlab and create a new simulink project, Only include a Counter Free-Running and a scope and configure according to the following tutorial:

https://nl.mathworks.com/help/robotics/examples/generate-a-standalone-ros-node-from-simulink.html

Make sure you only build the code and do not deploy it (this is also possible but we would like to understand the process here).

Now build the model [ctrl+b] and wait until it succeeds. This will generate the C++ code.

The next step is to create the ROS package so that we can build using catkin:

    ./build_ros_model.sh simulink_node_example_start.tgz ~/ros/kinetic/dev

If everything went well, we just compiled our Simulink ROS node, you can run the node:

    rosrun simulink_node_example_start simulink_node_example_start_node # you should see ** starting model **

Now connect via external mode in Simulink to your running node and see the counter increasing. 

To validate that it is indeed a ros node, check if the node is alive:

    rosnode list
    rosnode info [node-name]

You will see that it is not subscribing or publishing, let's change that! Go to Part 2!

### Part 2

Now create a new model (or copy) and make a node that publishes a std_msgs/Float32 with an increasing counter @ 1hz on the topic `counter`. Look at https://nl.mathworks.com/help/robotics/examples/get-started-with-ros-in-simulink.html for some documentation.

    # Do some work yourself :)

Now deploy your node as described in Part 1:

    # Run it :)

Now inspect the ROS network and you will see the `counter` topic, you can inspect this topic:

    rostopic echo /counter

### Part 3

Now we would like to create another node that subscribes to this `/counter` topic, and publish whether the counter is even or odd.
Make the node publish a `std_msgs/Bool` over the `/result` topic; `True' if the value is `even` and a `False` if the message is `odd`:

So copy or create a new model and realize this behavior:

    # Do some work yourself

Now deploy your node as described in Part 1:

    # Run it :)

And now rerun the node and display the message:

    rosrun python_node_example_start listener.py
    rostopic echo /result

Good job, you finished this tutorial! Now go connect the newly created ROS node with some other ROS node that you have created. Without connecting things to each other, there is no need using ROS!
