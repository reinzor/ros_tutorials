# ros_tutorial 

## Prerequisites

- ROS Installed on your system

To install ROS on your system, you can either use Ubuntu or a virtual machine. Either way, see http://wiki.ros.org/Installation 
After the installation, follow http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment 
If the last guide is done, you are set to do the hands-on part of the introduction. 
Doing this will take up to an hour or so. 

## Tutorials

Now you can start with the tutorials:

```bash
roscd && cd ../src
git clone git@gitlab.com:reinzor/ros_tutorials.git
```

1. [Working with bagfiles](1_bagfiles)
2. [ROS Node Python](2_ros_node_python)
3. [ROS Node Matlab Simulink](3_ros_node_simulink)
4. [ROS Node C++](4_ros_node_cpp)
5. [ROS Node Javascript](5_ros_node_js)
6. [ROS Bridge Javascript](6_ros_bridge_js)


